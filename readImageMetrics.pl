#!/usr/bin/perl -w

use strict;

my $file = shift;

open F, "<$file";

my $idx = 0;
my %dat = ();
my %cycles = ();
read(F, my $version, 1);
read(F, my $recln, 1);

while (1) {
	my @rec = ();	

	read(F, $rec[0], 2) || last;
	read(F, $rec[1], 2);
	read(F, $rec[2], 2);
	read(F, $rec[3], 2);
	read(F, $rec[4], 2);
	read(F, $rec[5], 2);

	my $lane = unpack("S", $rec[0]);
	my $tile = unpack("S", $rec[1]);
	my $cycle = unpack("S", $rec[2]);
	my $channel = unpack("S", $rec[3]);
	my $min = unpack("S", $rec[4]);
	my $max = unpack("S", $rec[5]);
	
	$dat{$lane}{$cycle}{$channel} = [$min, $max];
}

close F;

foreach my $lane (sort keys(%dat)) {
foreach my $cycle (sort { $a <=> $b } keys(%{$dat{$lane}})) {
foreach my $channel (sort { $a <=> $b } keys(%{$dat{$lane}{$cycle}})) {
	print "$lane - $cycle : " . join("::", @{$dat{$lane}{$cycle}{$channel}}) . "\n";

}}}
