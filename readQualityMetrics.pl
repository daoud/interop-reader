#!/usr/bin/perl -w

use strict;

my $file = shift;

open F, "<$file";

my $idx = 0;
my %dat = ();
my %cycles = ();
read(F, my $version, 1);
read(F, my $recln, 1);

while (1) {
	my @rec = ();	

	read(F, $rec[0], 2) || last;
	read(F, $rec[1], 2);
	read(F, $rec[2], 2);

	my $lane = unpack("S", $rec[0]);
	my $tile = unpack("S", $rec[1]);
	my $cycle = unpack("S", $rec[2]);
	$cycles{$lane}{$cycle} = 0;
	foreach my $q (0 ..28) {
		read(F, $rec[3], 4);
		$dat{$lane}{below} += unpack("L", $rec[3]);
	}
	foreach my $q (29 .. 49) {
		read(F, $rec[3], 4);
		$dat{$lane}{above} += unpack("L", $rec[3]);
	}
}

close F;

foreach my $lane (sort keys(%dat)) {
	my $ab = $dat{$lane}{above};
	my $be = $dat{$lane}{below};
	print $lane . " Cycles : " . scalar(keys(%{$cycles{$lane}})) . "\n";
	print $lane . " Q30 : " . ($ab / ($ab + $be) * 100) . " %\n";

}
