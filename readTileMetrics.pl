#!/usr/bin/perl -w

use strict;

my $file = shift;

open F, "<$file";

my $idx = 0;
my %dat = ();
read(F, my $version, 1);
read(F, my $recln, 1);

while (1) {
	my @rec = ();	

	read(F, $rec[0], 2) || last;
	read(F, $rec[1], 2);
	read(F, $rec[2], 2);
	read(F, $rec[3], 4);

	my $lane = unpack("S", $rec[0]);
	my $tile = unpack("S", $rec[1]);
	my $mcode = unpack("S", $rec[2]);
	my $mvalue = unpack("f", $rec[3]);

	#print join("\t", $lane, $tile, $mcode, $mvalue) . "\n"; sleep 1;

	push(@{$dat{$lane}{dclusters}}, $mvalue) if ($mcode == 100);
	push(@{$dat{$lane}{dclustersPF}}, $mvalue) if ($mcode == 101);
	$dat{$lane}{nclusters} += $mvalue if ($mcode == 102);
	$dat{$lane}{nclustersPF} += $mvalue if ($mcode == 103);
}

close F;

foreach my $lane (sort keys(%dat)) {
	my $tot = 0;
	$tot += $_ foreach (@{$dat{$lane}{dclusters}});
	my $totPF = 0;
	$totPF += $_ foreach (@{$dat{$lane}{dclustersPF}});
	print STDOUT $lane . "d:" . $tot / scalar(@{$dat{$lane}{dclusters}}) / 1000 . "\n";
	print STDOUT $lane . "dPF:" . $totPF / scalar(@{$dat{$lane}{dclustersPF}}) / 1000 . "\n";
	print STDOUT $lane . "n:" . $dat{$lane}{nclusters} . "\n";
	print STDOUT $lane . "nPF:" . $dat{$lane}{nclustersPF} . "\n";

}
