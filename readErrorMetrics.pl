#!/usr/bin/perl -w

use strict;

my $file = shift;

open F, "<$file";

my $idx = 0;
my %dat = ();
my %cycles = ();
read(F, my $version, 1);
read(F, my $recln, 1);

while (1) {
	my @rec = ();	

	read(F, $rec[0], 2) || last;
	read(F, $rec[1], 2);
	read(F, $rec[2], 2);
	read(F, $rec[3], 4);
	read(F, $rec[4], 4);
	read(F, $rec[5], 4);
	read(F, $rec[6], 4);
	read(F, $rec[7], 4);
	read(F, $rec[8], 4);

	my $lane = unpack("S", $rec[0]);
	my $tile = unpack("S", $rec[1]);
	my $cycle = unpack("S", $rec[2]);
	my $error = unpack("f", $rec[3]);
	my $errorp = unpack("L", $rec[4]);
	my $error1 = unpack("L", $rec[5]);
	my $error2 = unpack("L", $rec[6]);
	my $error3 = unpack("L", $rec[7]);
	my $error4 = unpack("L", $rec[8]);
	my $tot = $errorp + $error1 + $error2 + $error3 + $error4;
	$dat{$lane}{$cycle} = [$error, $errorp, $error1, $error2, $error3, $error4, $tot];
}

close F;

foreach my $lane (sort keys(%dat)) {
	foreach my $cycle (sort { $a <=> $b } keys(%{$dat{$lane}})) {
	print "$lane - $cycle : " . join("::", @{$dat{$lane}{$cycle}}) . "\n";

}}
